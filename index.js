const express = require("express")
var bodyParser = require('body-parser');
var multer = require('multer');
var upload = multer();
const sql = require("sqlite3").verbose()
let db = new sql.Database('db1')
app = express()
app.use(require('sanitize').middleware);
app.set('view engine', 'pug');
app.set('views', './views');
app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({ extended: true }));
app.use(upload.array());
app.use(express.static('public'));

// show details of bus
app.get("/bus", (req, res) => {
    db.all("select * from bus;", (err, result) => {
        if (err)
            res.status(500).send(err)
        else
            res.send(result)
    })
})
// add bus
app.post("/bus", (req, res) => {
    from = req.body["from"]
    to = req.body["to"]
    seats = req.body["seats"]
    // console.log(from,to,seats)
    // from,to,seats= req.body
    // console.log(from,to,seats)
    // return(0)
    if (from===undefined || to===undefined || seats===undefined){
        res.status(422).send("check variables")
        return(0)
    }
    query = `insert into bus('dep' , 'arrival' , 'seats') values('${from}','${to}','${seats}')`
    db.run(query, function sai(err){
        if (err) {
            res.status(500).send(err)
            return(0)
        }
        res.send(JSON.stringify(this.lastID, null, 3))
    })
})
// book ticket
app.post("/bus/:busid", (req, res) => {
    busid = req.params['busid']
    name = req.body['name']
    phone = req.body['phone']
    if (busid===undefined || name===undefined || phone===undefined){
        res.status(422).send("check variables")
        return(0)
    }
    db.all(`select seats from bus where busid=${busid};`, (err, result1) => {
        if (err) {
            res.status(500).send(err)
            return (0)
        }
        if (result1.length == 0) {
            res.status(500).send("check bus_id")
            return (0)
        }
        seatno = result1[0]['seats']
        if (seatno <= 0) {
            res.status(500).send("no seats avilable")
        } else {
            db.run(`update bus set seats=${seatno-1} where busid=${busid}`)
            db.all(`insert into details('busid','seatno','name','phone') values(${busid},${seatno},'${name}',${phone})`, (err) => {
                db.all("SELECT ticketid FROM details ORDER BY ticketid DESC LIMIT 1", (err1, result4) => {
                    if (err + err1) {
                        res.status(500).send(err + err1)
                        return (0)
                    }
                    ticketid = result4[0]['ticketid']
                    res.send(JSON.stringify(`${ticketid}`, null, 3))
                })
            })
        }

    })
})
// update ticket
app.put("/ticket/:ticketid", (req, res) => {   
    q = req.body
    ticketid = req.params['ticketid']
    s = ""
    for (let i in q) {
        s += `${i}='${q[i]}',`
    }
    s = s.substr(0, s.length - 1)
    query = `update details set ${s} where ticketid=${ticketid}`
    db.run(query, (err) => {
        if (err) {
            res.status(500).send(err)
            return (0)
        }
        res.send("done-1")
    })
})
// ticket status
app.get("ticket/:ticketid", (req, res) => {
    ticketid = req.params['ticketid']
    db.all(`select * from details where ticketid=${ticketid}`, (err, result) => {
        if (err) {
            res.status(500).send(err)
            return (0)
        }
        if (result.length == 0) {
            res.status(400).send("chech ticketid.")
            return (0)
        }
        console.log(result[0])
        res.send(JSON.stringify(result[0], null, 3))
    })
})
// openticket
app.get("/ticket/open", (req, res) => {
    db.all(`select * from details where open=1`, (err, result) => {
        if (err) {
            res.status(500).send(err)
            return (0)
        }
        res.send(JSON.stringify(result, null, 3))
    })
})
// closeticket
app.get("/ticket/close", (req, res) => {
    db.all(`select * from details where open=0`, (err, result) => {
        if (err) {
            res.status(500).send(err)
            return (0)
        }
        res.send(JSON.stringify(result, null, 3))
    })
})
// close bus
app.put("/closebus/:busid", (req, res) => {
    busid = req.params['busid']
    db.run(`update details set open=0 where busid=${busid}`, (err) => {
        if (err) {
            res.status(500).send(err)
            return (0)
        }
        res.send("done")
    })
})
app.listen(process.env.PORT || 5000)